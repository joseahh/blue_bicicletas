var map = L.map('main_map').setView([20.9660757, -89.5893967], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

/* L.marker([20.9660757, -89.5893967]).addTo(map);
L.marker([20.9621001, -89.5968967]).addTo(map);
L.marker([20.9607709, -89.5811489]).addTo(map);
*/

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result);
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
    }
})

