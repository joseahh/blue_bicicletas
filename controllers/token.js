var Token = require("../models/token");
var Usuario = require("../models/usuario");

module.exports = {
  confirmationGet: function (req, res, next) {
    Token.findOne({ token: req.params.token }, function (err, token) {
      if (!token)
        return req
          .status(400)
          .send({ type: "not-verified", msg: "No encontramos usuario con este token" });
      Usuario.findById(token._userId, function (err, usuario) {
        if (!usuario)
          return res.status(400).send({ msg: "No encontramos un usuario" });
        if (usuario.verificado) return res.redirect("/usuarios");
        usuario.verificado = true;
        usuario.save(function (err) {
          if (err) { return res.status(500).send({ msg: err.message }); }
          res.redirect("/");
        });
      });
    });
  },
};



/*
const Usuario = require('../models/usuario')
const Token = require('../models/token')

module.exports = {
    confirmationGet: function(req, res, next){
        Token.findOne({token: req.params.token}, (err, token) => {
            if(!token){
                return res.status(400).send({type: 'not-verified', msg: 'No existe usuario con este token'})
            }
            Usuario.findById(token._userId, (err, usuario) => {
                if(!usuario) return res.status(400).send({msg: 'No hay usuario con esta id'});
                if(usuario.verificado) return res.redirect('/usuarios');
                usuario.verificado = true;
                usuario.save((err) => {
                    if(err) { return res.status(500).send({msg: err.message}); }
                    res.redirect('/');
                });
            });
        });
    },
}
*/